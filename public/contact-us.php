<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Knowledge Partners | Contact Us</title>

	<!-- include head content -->
	<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/head-content.php'); ?>

	<!-- google maps api -->
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
</head>
<body>
	<!-- include header -->
	<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/header.php'); ?>

	<!-- top mini slider -->
	<div id="main-content-container">
		<div id="top-slider" class="mini-slider flexslider flexslider-background-image">
			<ul class="slides">
				<li style="background-image: url(/images/stock/team-work.jpg)">
					<div class="container slider-content">
						<div class="row">
							<div class="col-sm-6">
								<h2>Lorem ipsum dolor sit amet.</h2>
							</div>
							<div class="col-sm-6">
								<p>Sed porttitor lectus nibh. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>

		<!-- page title -->
		<div id="page-title">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h1>Contact</h1>
					</div>
				</div>
			</div>
		</div>

		<!-- contact us content -->
		<div class="container">
			<div class="row">
				<div class="col-md-3" id="content-sub-contacts">
					<ul>
						<li>
							<h3>Address 1</h3>
							<address>
								123, Lorem ipsum dolor.<br>
								Lorem ipsum dolor sit amet. <br>
								Sri Lanka
							 </address>
						</li>

						<li>
							<h3>Address 1</h3>
							<address>
								123, Lorem dolor.<br>
								sit amet. <br>
								USA
							 </address>
						</li>

						<li>
							<h3>Telephone</h3>
							<address>
								+94 0112 123456<br>
								+94 0778 098765
							</address>
						</li>
					</ul>
				</div>
				<div class="col-md-9" id="content-display-area">
					<article>
						<h1>Drop Us A Message</h1>
						<div class="in-page-banner background-overlay" style="background-image: url(/images/graphic/orange-man.jpg)">
							<div class="inner-banner-container">
								<h2><span class="highlight highlight-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo, impedit!</span></h2>
								<p><i>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit omnis nobis culpa excepturi optio magni voluptatum, non repellendus ipsam accusamus nulla cumque harum debitis quis deleniti, totam ex. Nobis, dicta!</i></p>
							</div>
						</div>
						<div id="form-container">
							<form action="">
								<div class="input-group">
									<label for="contact-form-name">Name:</label>
									<input type="text" id="contact-form-name" name="name" placeholder="name">
								</div>

								<div class="input-group">
									<label for="contact-form-email">Email:</label>
									<input type="email" id="contact-form-email" name="email" placeholder="email">
								</div>

								<div class="input-group">
									<label for="contact-form-telephone">Telephone:</label>
									<input type="text" id="contact-form-telephone" name="telephone" placeholder="telephone">
								</div>

								<div class="input-group">
									<label for="contact-form-message">Message:</label>
									<textarea name="message" id="contact-form-message" placeholder="message"></textarea>
								</div>

								<button id="contact-form-submit" type="submit">Send!</button>
							</form>
						</div>
					</article>
				</div>
			</div>
		</div>
		<div id="map-canvas">
			<!-- google maps -->
		</div>
	</div>
	<!-- include footer -->
	<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/footer.php'); ?>
</body>
<!-- include scripts -->
<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/scripts.php'); ?>
</html>