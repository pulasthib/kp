$(document).ready(function(){
	$('#top-slider').flexslider({
		animation: 'slide',
	});

	$('#testimonial-slider').flexslider({
		animation: 'fade',
		directionNav: false,  
	});

	$('#top-navigation-logo').click(function(){
		$('body').toggleClass('nav-active');
	});
});


var googleMaps = {};
googleMaps.initializeMaps = function() {
	googleMaps.location = new google.maps.LatLng(6.909856, 79.854059);

	googleMaps.mapOptions = {
		center: googleMaps.location,
		zoom: 15
	};
	googleMaps.map = new google.maps.Map(document.getElementById('map-canvas'), googleMaps.mapOptions);	

  	googleMaps.info = {};
  	googleMaps.info.title = 'Knowledge Partners';
  	googleMaps.info.address = '123, Lorem ipsum dolor. \
  		Lorem ipsum dolor sit amet.</br>\
  		Sri Lanka</br></br>\
  		+94 0112 123456, \
		+94 0778 098765';

  	googleMaps.info.html = '<h3>'+googleMaps.info.title+'</h3>'+
  							'<address>'+googleMaps.info.address+'</address>';

	googleMaps.infoWindow = new google.maps.InfoWindow({
		content: googleMaps.info.html
	});

	//set marker control funcions
	googleMaps.setLocationMarker = function() {
			googleMaps.locationMarker = new google.maps.Marker({
				position: googleMaps.location,
				map: googleMaps.map,
				title: 'Knowledge Partners',
		  	});

		  	googleMaps.infoWindow.open(googleMaps.map, googleMaps.locationMarker);
	};

	googleMaps.setLocationMarker();
}

google.maps.event.addDomListener(window, 'load', googleMaps.initializeMaps);