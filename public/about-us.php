<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Knowledge Partners | About Us</title>

	<!-- include head content -->
	<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/head-content.php'); ?>
</head>
<body>
	<!-- include header -->
	<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/header.php'); ?>

	<!-- top mini slider -->
	<div id="main-content-container">
		<div id="top-slider" class="mini-slider flexslider flexslider-background-image">
			<ul class="slides">
				<li style="background-image: url(/images/stock/class-room.jpg)">
					<div class="container slider-content">
						<div class="row">
							<div class="col-sm-6">
								<h2>Lorem ipsum dolor sit amet.</h2>
							</div>
							<div class="col-sm-6">
								<p>Sed porttitor lectus nibh. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>

		<!-- page title -->
		<div id="page-title">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h1>About Us</h1>
					</div>
				</div>
			</div>
		</div>

		<!-- about us content -->
		<div class="container">
			<div class="row">
				<div class="col-md-3" id="content-sub-navigation">
					<h3>About Knowledge Partners</h3>
					<ul>
						<li class="active"><a href="/about-us.php">Knowledge Partners</a></li>
						<li><a href="/our-philosophy.php">Our Philosophy</a></li>
					</ul>
				</div>
				<div class="col-md-9" id="content-display-area">
					<article>
						<h1>Knowledge Partners</h1>
						<p>Knowledge Partners will offer affordable, tailored, cost effective, higher and tertiary-education services to the youth of Sri Lanka serving their individual needs and also indirectly the needs of the country.</p>
						<h3>Our Mission</h3>
						<blockquote>We wish to create a space where education in Sri Lanka becomes an equitable pursuit, common and accessible to all who seek and strive after knowledge; a space where discovery, critical thought and free expression are fostered, encouraged and championed; a space where productive young people can realize their full intellectual potential; a space where these young people can also pass on, to those who follow, the same values that were fostered in them, along with the thirst for knowledge, under our care.</blockquote>
						<h3>Focus, Method, Objectives & Principles</h3>
						<p>While the problems we have chosen to address have massive consequences at national level, tackling national questions head-on is beyond our remit. Instead, we have chosen a specific area in which we are sure we can deliver social value. On it we shall concentrate.</p>
						<p><b>Our focus</b>, then, will be on the student, whose interests will at all times be paramount in our concerns, plans and activities. </p>
						<p><b>Our method</b>, then, will be to extend appropriate and affordable higher and tertiary-education services to young people: services that will serve their individual needs and also, indirectly, those of the country.</p>
						<h3>Our objectives</h3>
						<ol>
							<li>To help students maximize the return on their investment in higher education</li>
							<li>To broaden and diversify the range of skills and qualifications (particularly vocational) on offer, doing so by selecting courses and qualifications which will stand our students in good stead in the job market at home and abroad</li>
							<li>To achieve financial independence and sustainability in our activities.</li>
						</ol>

						<div class="in-page-banner background-overlay" style="background-image: url(/images/graphic/purple-men.jpg)">
							<div class="inner-banner-container">
								<h2><span class="highlight highlight-white">We are social entrepreneurs</span></h2>
								<p><i>pursuing an innovative idea that we believe has the potential to solve a community problem. We are willing to take on the risk and effort demanded of those who wish to work a positive change in society.</i></p>
							</div>
						</div>

						<h3>Our principles</h3>
						<ol>
							<li>We will strive for excellence in all that we do. We value excellence and will appropriately apply it in teaching, research, and applied scholarship. We will deliver excellence in service that goes beyond the classroom, yielding enduring benefits to students, the campus community, and the world.  </li>
							<li>We will provide leadership, dedicating ourselves to the success of our students and facilitating their development into the leaders of tomorrow; leaders who, when their turn comes at the levers of power, will recognize the importance of knowledge and its promotion through education, training and skills development</li>
							<li>To innovate in everything we do, actively seeking out the development and implementation of new research directions, programmes, and partnerships that will enhance and make more relevant the educational experience of our students. </li>
						</ol>
						<p></p>
					</article>
				</div>
			</div>
		</div>
	</div>
	<!-- include footer -->
	<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/footer.php'); ?>
</body>
<!-- include scripts -->
<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/scripts.php'); ?>
</html>