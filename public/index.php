<!DOCTYPE html>
<html lang="en">
<head>
	<title>Knowledge Partners</title>

	<!-- include head content -->
	<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/head-content.php'); ?>
</head>
<body>
	<!-- include header -->
	<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/header.php'); ?>
	<div id="main-content-container">
		<!-- top slider -->
		<div id="top-slider" class="flexslider flexslider-background-image">
			<ul class="slides">
				<li style="background-image: url(/images/stock/class-room.jpg)">
					<div class="container slider-content">
						<div class="row">
							<div class="col-md-6">
								<h2>Lorem ipsum dolor sit amet.</h2>
							</div>
							<div class="col-md-6">
								<p>Sed porttitor lectus nibh. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
							</div>
						</div>
					</div>
				</li>
				<li style="background-image: url(/images/stock/student-activity.jpg)">
					<div class="container slider-content">
						<div class="row">
							<div class="col-md-6">
								<h2>Consectetur adipisicing elit. Est, fuga!</h2>
							</div>
							<div class="col-md-6">
								<p>Curabitur aliquet quam id dui posuere blandit. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Cras ultricies ligula sed magna dictum porta.</p>
							</div>
						</div>
					</div>
				</li>
				<li style="background-image: url(/images/stock/students.jpg)">
					<div class="container slider-content">
						<div class="row">
							<div class="col-md-6">
								<h2>Commodi quisquam eveniet nam ab.</h2>
							</div>
							<div class="col-md-6">
								<p>Curabitur aliquet quam id dui posuere blandit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque,</p>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>
		
		<!-- home intro section -->
		<section id="home-intro">
			<div class="container">
				<div class="row full-height-row">
					<div class="col-md-8" id="intro-services-wrapper">
						<div id="intro-services">
							<h1 class="highlight">Our Services: Lorem ipsum dolor.</h1>
							<div class="row">
								<ul class="icon-list">
									<li class="col-sm-6">
										<i class="fa fa-graduation-cap"></i>
										<h4>Lorem ipsum dolor</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta fugit molestiae laborum cumque consequatur nihil autem quas nam rem alias!</p>
									</li>
									<li class="col-sm-6">
										<i class="fa fa-university"></i>
										<h4>Consectetur adipisicing elit.</h4>
										<p>Praesentium fugiat quaerat odio, omnis distinctio est error, numquam voluptate molestias.</p>
									</li>
									<li class="clearfix visible-xs-block"></li>
									<li class="col-sm-6">
										<i class="fa fa-bar-chart"></i>
										<h4>Dolor sit amet, consectetur.</h4>
										<p>At facere repellat animi quis consequuntur totam, excepturi nobis.</p>
									</li>
									<li class="col-sm-6">
										<i class="fa fa-globe"></i>
										<h4>Adipisicing elit. Ipsam, at.</h4>
										<p>Sequi incidunt necessitatibus, odit eligendi ut dolore voluptate libero tempore officiis, inventore itaque possimus non dolorem aliquam id animi expedita!</p>
									</li>
									<li class="clearfix visible-xs-block"></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4" id="intro-feature-wrapper">
						<div id="intro-feature">
							<h2>We at Knowledge Parterns</h2>
							<p>Knowledge Partners seek to provide ethically sound, cost effective, student centric educational services to well deserving students seeking opportunities in higher education locally and internationally.</p>
							<p>Our focus will always be the student; student’s interest will be paramount to Knowledge Partners.</p>
							<p>Knowledge Partners will offer affordable, tailored, cost effective, higher and tertiary-education services to the youth of Sri Lanka serving their individual needs and also indirectly the needs of the country.  </p>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<!-- home testimonials section -->
		<section id="home-testimonials" class="background-image background-overlay" style="background-image: url(/images/stock/team-work.jpg)">
			<div class="container">
				<div class="row">
					<div id="testimonial-slider" class="flexslider flexslider-background-image">
						<ul class="slides">
							<li>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam praesentium dolores nobis, saepe eligendi officiis! Quod aliquam odit laborum. Quaerat.</p>
							</li>
							<li>
								<p>Temporibus eos at libero laboriosam, repudiandae nihil expedita totam asperiores voluptas suscipit veniam autem dicta adipisci, odio quia. Culpa, voluptatem.</p>
							</li>
							<li>
								<p>Aspernatur natus quidem dolore illo, excepturi quas sint sit ullam quod. Accusamus enim, quidem laboriosam iure omnis, eum saepe blanditiis!</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		
		<!-- home news section -->
		<section id="home-news">
			<div class="container">
				<div class="row full-height-row">
					<div class="col-md-4" id="news-list">
						<h1><i class="fa fa-newspaper-o"></i> Resent News</h1>
						<ul>
							<li>
								<article>
									<h3><a href="">Lorem ipsum dolor sit amet.</a></h3>
									<div class="article-date">25th Aug 2015</div>
									<p class="article-summary">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex debitis quaerat qui tempora temporibus exercitationem.</p>
								</article>
							</li>
							<li>
								<article>
									<h3><a href="">Nesciunt quam veniam fugit consequuntur.</a></h3>
									<div class="article-date">25th Aug 2015</div>
									<p class="article-summary">Deserunt iusto obcaecati tenetur non soluta, id veniam. Commodi, explicabo ullam eaque magnam ad, dolor.</p>
								</article>
							</li>
							<li>
								<article>
									<h3><a href="">A consectetur harum ducimus tenetur.</a></h3>
									<div class="article-date">25th Aug 2015</div>
									<p class="article-summary">Eos reiciendis dolorum praesentium, facere hic quae impedit, ea, soluta laborum iste deserunt nulla eum.</p>
								</article>
							</li>
							<li>
								<article>
									<h3><a href="">Architecto iusto illum, assumenda impedit.</a></h3>
									<div class="article-date">25th Aug 2015</div>
									<p class="article-summary">Aspernatur aliquid excepturi, iure, perferendis, explicabo laboriosam eaque, itaque illum sequi perspiciatis dolore cum facere.</p>
								</article>
							</li>
						</ul>
					</div>
					<div class="col-md-8" id="featured-article">
						<article>
							<h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero, aut!</h2>
							<div class="article-date">23rd August 2015</div>
							<p class="article-summary ">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni deleniti doloribus sequi blanditiis repudiandae sapiente et corrupti atque aliquam, aspernatur quibusdam vero, similique soluta aliquid veniam vel distinctio odit praesentium! <a href="">read more</a> </p>
							<div class="featured-image background-image" style="background-image: url(/images/stock/student-activity.jpg)">
								<div class="background-overlay-link">
									<a href=""><i class="fa fa-link fa-3x"></i></a>
								</div>
							</div>
						</article>
					</div>
				</div>
			</div>
		</section>
	</div>

	<!-- include footer -->
	<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/footer.php'); ?>
</body>
<!-- include scripts -->
<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/scripts.php'); ?>
</html>