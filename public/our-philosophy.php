<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Knowledge Partners | About Us</title>

	<!-- include head content -->
	<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/head-content.php'); ?>
</head>
<body>
	<!-- include header -->
	<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/header.php'); ?>

	<!-- top mini slider -->
	<div id="main-content-container">
		<div id="top-slider" class="mini-slider flexslider flexslider-background-image">
			<ul class="slides">
				<li style="background-image: url(/images/stock/team-work.jpg)">
					<div class="container slider-content">
						<div class="row">
							<div class="col-sm-6">
								<h2>Lorem ipsum dolor sit amet.</h2>
							</div>
							<div class="col-sm-6">
								<p>Sed porttitor lectus nibh. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>

		<!-- page title -->
		<div id="page-title">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h1>About Us</h1>
					</div>
				</div>
			</div>
		</div>

		<!-- about us content -->
		<div class="container">
			<div class="row">
				<div class="col-md-3" id="content-sub-navigation">
					<h3>About Knowledge Partners</h3>
					<ul>
						<li><a href="/about-us.php">Knowledge Partners</a></li>
						<li class="active"><a href="/our-philosophy.php">Our Philosophy</a></li>
					</ul>
				</div>
				<div class="col-md-9" id="content-display-area">
					<article>
						<h1>Our Philosophy</h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, explicabo expedita molestias labore facilis soluta magni debitis, vero in culpa quae aut laborum excepturi modi autem hic accusantium odio, quasi at nihil. Quod natus accusamus eaque praesentium facilis, aliquam distinctio!</p>
						<div class="in-page-banner background-overlay" style="background-image: url(/images/graphic/orange-man.jpg)">
							<div class="inner-banner-container">
								<h2><span class="highlight highlight-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo, impedit!</span></h2>
								<p><i>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit omnis nobis culpa excepturi optio magni voluptatum, non repellendus ipsam accusamus nulla cumque harum debitis quis deleniti, totam ex. Nobis, dicta!</i></p>
							</div>
						</div>
						<h3>Our Mission</h3>
						<blockquote>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet, distinctio officia nihil repellat rem, fugiat suscipit debitis, nobis fuga perferendis cupiditate ipsam laboriosam totam ut iure molestias placeat necessitatibus facilis. Nobis et dolor optio cumque, perspiciatis quidem, ullam consectetur ratione ea accusamus! Libero aliquid, voluptates.</blockquote>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt harum quisquam atque excepturi fuga totam quidem, quo at consequatur voluptatem error maiores non, rem ducimus dolorem et, beatae autem dicta.</p>
						<ul>
							<li>Lorem ipsum dolor sit amet, consectetur.</li>
							<li>Quam temporibus eligendi quia labore accusamus.</li>
							<li>Consequatur tenetur mollitia dolorem repellat modi!</li>
							<li>Quidem, ut quis? Consequatur ipsam, enim.</li>
							<li>Excepturi dolorum inventore laudantium a consequuntur!</li>
						</ul>
						<h3>Our Vision</h3>
						<blockquote>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero provident repellendus ipsam ipsa adipisci assumenda. Enim repellat hic similique porro eos maiores. Error numquam nihil reiciendis ex alias, magni, perspiciatis.</blockquote>
						<img src="/images/stock/class-room.jpg" alt="">
						<p class="image-caption">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ratione voluptatibus natus hic possimus animi, fuga laudantium laboriosam sint dolorem, expedita accusantium nisi. Ducimus voluptas repellat ratione accusamus adipisci doloribus, cum rem neque itaque, odit aliquid distinctio molestiae doloremque consequuntur?</p>
						
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate cum, voluptatem totam, non hic iusto similique dolore alias suscipit necessitatibus debitis. Minima explicabo consectetur perferendis magni praesentium eum fugiat, quod fuga velit nihil voluptatem possimus distinctio autem. Odio adipisci et ad unde sint. Nobis aspernatur rem eaque, praesentium, eius voluptates alias possimus blanditiis obcaecati. Quae suscipit animi eos, quas eaque.</p>
					</article>
				</div>
			</div>
		</div>
	</div>
	<!-- include footer -->
	<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/footer.php'); ?>
</body>
<!-- include scripts -->
<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/scripts.php'); ?>
</html>