<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Knowledge Partners | News</title>

	<!-- include head content -->
	<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/head-content.php'); ?>
</head>
<body>
	<!-- include header -->
	<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/header.php'); ?>

	<!-- top mini slider -->
	<div id="main-content-container">
		<div id="top-slider" class="mini-slider flexslider flexslider-background-image">
			<ul class="slides">
				<li style="background-image: url(/images/stock/student-activity.jpg)">
					<div class="container slider-content">
						<div class="row">
							<div class="col-sm-6">
								<h2>Lorem ipsum dolor sit amet.</h2>
							</div>
							<div class="col-sm-6">
								<p>Sed porttitor lectus nibh. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>

		<!-- page title -->
		<div id="page-title">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h1>News</h1>
					</div>
				</div>
			</div>
		</div>

		<!-- about us content -->
		<div class="container">
			<div class="row">
				<div class="col-md-3" id="content-sub-navigation">
					<h3>News and Events</h3>
					<ul>
						<li class="active"><a href="">All</a></li>
						<li><a href="">News</a></li>
						<li><a href="">Events</a></li>
						<li><a href="">Opportunities</a></li>
					</ul>
				</div>
				<div class="col-md-9" id="news-list-area">
					<ul>
						<li>
							<article>
								<div class="news-featured-image"></div>
								<h2 class="news-title"><a href="">Lorem ipsum dolor sit amet.</a></h2>
								<div class="news-date">24th Aug 2014</div>
								<div class="news-summary">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores ab, impedit at fugit ducimus tempore asperiores nisi molestias inventore porro temporibus rerum eveniet fugiat dolorem corrupti sed nostrum sapiente? Non tenetur, accusantium illo! Rerum ullam inventore tenetur quo porro dicta ut, molestias officia, cumque suscipit tempore totam eos voluptatem sint!</div>
								<a class="news-readmore" href="">read more</a>
							</article>
						</li>
						<li>
							<article>
								<div class="news-featured-image"></div>
								<h2 class="news-title"><a href="">Atque maiores, illo necessitatibus sed.</a></h2>
								<div class="news-date">24th Aug 2014</div>
								<div class="news-summary">Dolores libero aliquam ullam eaque necessitatibus porro officia distinctio, fugit, impedit, facilis ipsum, consectetur nisi ducimus saepe facere cum explicabo modi perspiciatis incidunt veniam aperiam obcaecati assumenda. Distinctio officia quisquam sint quos aliquid, doloremque sunt id facere ex eligendi animi placeat voluptate a, sed esse similique itaque, dicta quia ullam.</div>
								<a class="news-readmore" href="">read more</a>
							</article>
						</li>
						<li>
							<article>
								<div class="news-featured-image"></div>
								<h2 class="news-title"><a href="">Dignissimos expedita eos similique nostrum.</a></h2>
								<div class="news-date">24th Aug 2014</div>
								<div class="news-summary">Voluptatem, dolor nostrum quibusdam, molestiae maiores, quidem rerum corporis enim ipsam laboriosam similique exercitationem expedita placeat, debitis modi. Sit aperiam nesciunt ipsa magnam, atque assumenda necessitatibus. Quisquam maiores quam, nihil odio perspiciatis aliquam officia reiciendis harum numquam minus laboriosam distinctio itaque illo totam, vitae reprehenderit est similique error eveniet ab.</div>
								<a class="news-readmore" href="">read more</a>
							</article>
						</li>
						<li>
							<article>
								<div class="news-featured-image"></div>
								<h2 class="news-title"><a href="">Delectus iure optio quidem vitae?</a></h2>
								<div class="news-date">24th Aug 2014</div>
								<div class="news-summary">Ipsa aliquam natus, nam earum non laudantium eligendi eos, beatae ipsum esse labore molestiae tenetur aperiam explicabo. Vero veritatis vitae obcaecati dolore quis corrupti omnis in ratione dignissimos asperiores, eaque debitis officiis delectus ex unde quod cumque incidunt modi iste. Ipsum molestias nesciunt illo obcaecati dolores iusto aliquid possimus sint?</div>
								<a class="news-readmore" href="">read more</a>
							</article>
						</li>
						<li>
							<article>
								<div class="news-featured-image"></div>
								<h2 class="news-title"><a href="">Amet consequatur veritatis optio saepe.</a></h2>
								<div class="news-date">24th Aug 2014</div>
								<div class="news-summary">Similique incidunt hic ullam voluptas laborum iste culpa voluptates quae labore, pariatur molestiae necessitatibus beatae soluta, unde officia provident placeat. Eos mollitia dolorem numquam voluptate facilis aut ratione sapiente excepturi quos porro doloribus illum consequatur deleniti sit officia, culpa voluptatibus sequi id recusandae! Delectus, laboriosam cum temporibus, autem quibusdam praesentium.</div>
								<a class="news-readmore" href="">read more</a>
							</article>
						</li>
					</ul>
				</div>
				<div class="clearfix"></div>
				<div class="pagination-container col-md-12">
					<ul class="clearfix">
						<li><a href="">5</a></li>
						<li><a href="">4</a></li>
						<li><a href="">3</a></li>
						<li><a href="">2</a></li>
						<li class="active"><a href="">1</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- include footer -->
	<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/footer.php'); ?>
</body>
<!-- include scripts -->
<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/scripts.php'); ?>
</html>