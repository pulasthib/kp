<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Knowledge Partners | Courses</title>

	<!-- include head content -->
	<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/head-content.php'); ?>
</head>
<body>
	<!-- include header -->
	<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/header.php'); ?>

	<!-- top mini slider -->
	<div id="main-content-container">
		<div id="top-slider" class="mini-slider flexslider flexslider-background-image">
			<ul class="slides">
				<li style="background-image: url(/images/stock/students.jpg)">
					<div class="container slider-content">
						<div class="row">
							<div class="col-sm-6">
								<h2>Lorem ipsum dolor sit amet.</h2>
							</div>
							<div class="col-sm-6">
								<p>Sed porttitor lectus nibh. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>

		<!-- page title -->
		<div id="page-title">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h1>Courses</h1>
					</div>
				</div>
			</div>
		</div>

		<!-- about us content -->
		<div class="container">
			<div class="row">
				<div class="col-md-3" id="content-sub-navigation">
					<h3>Disciplines</h3>
					<ul>
						<li class="active"><a href="">All</a></li>
						<li><a href="">Information Technology</a></li>
						<li><a href="">Biology</a></li>
						<li><a href="">Financial Administration</a></li>
						<li><a href="">Visual Arts</a></li>
						<li><a href="">Law</a></li>
						<li><a href="">Natural Sciences</a></li>
					</ul>
				</div>
				<div class="col-md-9" id="course-list-area">
					<ul class="row">
						<li class="course-item col-sm-6">
							<div class="course-item-container">
								<div class="institute-logo-wrapper">
									<div class="institute-logo" style="background-image:url(/images/universities/1.jpg)"></div>
								</div>
								<div class="course-name">Masters in Information Technology</div>
								<div class="institute-name">Lorem University.</div>
								<div class="background-overlay-link">
									<a href=""><i class="fa fa-link fa-3x"></i></a>
								</div>
							</div>
						</li>
						<li class="course-item col-sm-6">
							<div class="course-item-container">
								<div class="institute-logo-wrapper">
									<div class="institute-logo" style="background-image:url(/images/universities/2.jpg)"></div>
								</div>
								<div class="course-name">Higher Deploma in Visual Arts</div>
								<div class="institute-name">Univresity of Ipsum dolor</div>
								<div class="background-overlay-link">
									<a href=""><i class="fa fa-link fa-3x"></i></a>
								</div>
							</div>
						</li>
						<li class="clearfix"></li>
						<li class="course-item col-sm-6">
							<div class="course-item-container">
								<div class="institute-logo-wrapper">
									<div class="institute-logo" style="background-image:url(/images/universities/3.jpg)"></div>
								</div>
								<div class="course-name">Degree in Web Design and Development</div>
								<div class="institute-name">Dolor sit.</div>
								<div class="background-overlay-link">
									<a href=""><i class="fa fa-link fa-3x"></i></a>
								</div>
							</div>
						</li>
						<li class="course-item col-sm-6">
							<div class="course-item-container">
								<div class="institute-logo-wrapper">
									<div class="institute-logo" style="background-image:url(/images/universities/1.jpg)"></div>
								</div>
								<div class="course-name">Post Graduate Diploma in Financial Administration</div>
								<div class="institute-name">Harward Business School</div>
								<div class="background-overlay-link">
									<a href=""><i class="fa fa-link fa-3x"></i></a>
								</div>
							</div>
						</li>
						<li class="clearfix"></li>
						<li class="course-item col-sm-6">
							<div class="course-item-container">
								<div class="institute-logo-wrapper">
									<div class="institute-logo" style="background-image:url(/images/universities/4.jpg)"></div>
								</div>
								<div class="course-name">Post Graduate Diploma in Information Technolgy and Information Systems</div>
								<div class="institute-name">Sit amet.</div>
								<div class="background-overlay-link">
									<a href=""><i class="fa fa-link fa-3x"></i></a>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="clearfix"></div>
				<div class="pagination-container col-md-12">
					<ul class="clearfix">
						<li><a href="">5</a></li>
						<li><a href="">4</a></li>
						<li><a href="">3</a></li>
						<li><a href="">2</a></li>
						<li class="active"><a href="">1</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- include footer -->
	<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/footer.php'); ?>
</body>
<!-- include scripts -->
<?php include($_SERVER['DOCUMENT_ROOT'].'/../app/includes/scripts.php'); ?>
</html>