var gulp = require('gulp');
var scss = require('gulp-sass');
var source = require('vinyl-source-stream');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var autoprefixer = require('gulp-autoprefixer');
var minifyCss = require('gulp-minify-css');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');


gulp.task('compile:scss', function () {
  gulp.src('./source/scss/*.scss')
    .pipe(scss.sync().on('error', scss.logError))
    .pipe(autoprefixer())
    .pipe(gulp.dest('./public/css'))
    .pipe(minifyCss())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('./public/css'));;

	console.log('scss compiled');
});
 
gulp.task('scss:watch',['compile:scss'], function () {
  gulp.watch('./source/scss/**/*.scss', ['compile:scss']);
});


gulp.task('compile:js', function(){
	gulp.src(['./source/js/**/*.js'])
	.pipe(gulp.dest('./public/js'))
	.pipe(rename({suffix: '.min'}))
    .pipe(uglify().on('error', gutil.log))
    .pipe(gulp.dest('./public/js'))

	console.log('javascript compiled');
});


gulp.task('js:watch',['compile:js'], function(){
	gulp.watch('./source/js/**/*.js', ['compile:js']);
})

gulp.task('watch:dev', ['scss:watch', 'js:watch']);
