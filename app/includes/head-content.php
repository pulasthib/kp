<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- fonts -->
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300italic,300|Raleway:900,600' rel='stylesheet' type='text/css'>
<!-- bootstrap grid only -->
<link rel="stylesheet" href="/css/bootstrap.grid.min.css">
<!-- flex slider -->
<link rel="stylesheet" href="/bower_components/flexslider/flexslider.css">
<!-- font awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- custom styles -->
<link rel="stylesheet" href="/css/styles.css">