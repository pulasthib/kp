<!-- footer -->
<footer>
	<div class="container">
		<div class="row full-height-row">
			<div class="col-md-4" id="footer-navigation">
				<ul>
					<li><a href="">Home</a></li>
					<li><a href="">About Us</a></li>
					<li><a href="">News</a></li>
					<li><a href="">Courses</a></li>
					<li><a href="">Contact Us</a></li>
				</ul>
				<div id="social-link-container">
					<a href=""><i class="fa fa-facebook-square"></i></a>
					<a href=""><i class="fa fa-twitter-square"></i></a>
					<a href=""><i class="fa fa-linkedin-square"></i></a>
					<a href=""><i class="fa fa-google-plus-square"></i></a>
				</div>
			</div>
			<div class="col-md-4" id="footer-message">
				<div id="footer-logo-container" style="background-image: url(/images/logo.png)"></div>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, alias deleniti sapiente in non voluptas vero officia, id pariatur error. Totam tenetur, quia pariatur voluptate temporibus.</p>
			</div>
			<div class="col-md-4" id="footer-contact">
				<address>
					Anthony Benoit, 490 E <br> Main Street <br> Norwich CT 06360
				</address>
				<br>
				+94 779 801801 
				<br>
				+94 112 729729
			</div>
		</div>
	</div>
	<div id="footer-copyright">
		Copyright © 2015. All Rights Reserved.
	</div>
</footer>