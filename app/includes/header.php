<!-- header section -->
<header>
	<!-- top navigation -->
	<nav id="top-navigation">
		<div class="container">
			<!-- top navigatio logo -->
			<div id="top-navigation-logo" class="col-md-3 col-sm-2"></div>
			<!-- top navigation links -->
			<div id="top-navigation-links" class="col-md-9 col-sm-10">
				<ul>
					<li class="active">
						<a href="/">
							<h4>Home</h4>
							<h5>Lorem ipsum.</h5>
						</a>
					</li>
					<li>
						<a href="/about-us.php">
							<h4>About Us</h4>
							<h5>Amet, consectetur</h5>
						</a>
					</li>
					<li>
						<a href="/news.php">
							<h4>News</h4>
							<h5>Adipisicing elit</h5>
						</a>
					</li>
					<li>
						<a href="/courses.php">
							<h4>Courses</h4>
							<h5>Ducimus, sequi</h5>
						</a>
					</li>
					<li>
						<a href="/contact-us.php">
							<h4>Contact Us</h4>
							<h5>Dolor sit amet.</h5>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>